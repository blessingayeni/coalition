<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderFormController extends Controller
{
    //
    public function getOrderForm()
    {
        return view('order-form');
    }

    public function postOrderForm(Request $request)
    {
        $order = new Order();
        $order->product_name = $request->input('product_name');
        $order->quantity_in_stock = $request->input('quantity_in_stock');
        $order->price = $request->input('price');
        $order->total = $request->input('quantity_in_stock') * $request->input('price');
        $order->save();
        return response()->json($order);
    }
}
