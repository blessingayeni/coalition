<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
        <title>Order Form</title>
    </head>
    <body>
    <div class="container">
        <h2>Order Form</h2>
        <form>
            <div class="form-group">
                <label for="productName">Product name</label>
                <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name">
            </div>
            <div class="form-group">
                <label for="quantityInStock">Quantity in stock</label>
                <input type="text" class="form-control" id="quantity_in_stock" name="quantity_in_stock" placeholder="Enter Quantity in stock">
            </div>
            <div class="form-group">
                <label for="price">Price per item</label>
                <input type="text" class="form-control" id="price" name="price" placeholder="Enter Price">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <br><br>

        <div class="table-responsive" id="myOrder" style="display: none">
            <table class="table">
                <tr>
                    <th>Product name</th>
                    <th>Quantity in stock</th>
                    <th>Price per item</th>
                    <th>Datetime submitted</th>
                    <th>Total value number</th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>

    </div>



    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script>
        $.ajax({
            url: "order",
            method: "POST",
            data: $('#order-form').serialize(),
            success:function(data)
            {
                $('#insert-form')[0].reset();
                doccument.getElementById('myOrder').style.display="block";
                $('#successMessage').html(successContent);
                $('#registrationModal').delay(3000).fadeOut(350);

            }
        });
    </script>
    </body>

</html>